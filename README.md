### Migration case study ###

I have provided two solutions for the database migration script. The bash one works fine. It takes the following steps;

1. set script constants. Here you can specify the username, password, database and migratio directory as constants.
2. Querying the database for the current version"
3. Iterate over every .sql script in the directory. 
4. Check and apply the migrations by using a regex to extract the version from the start of the filename.
5. If the script version is greater than the dbversion, run the script against the database.
6. If the script version is greater than maxdbversion set that to the scriptversion
7. Continue till all scripts have been processed
8. Write maxdbversion to the database as the new database version.


The other user case, the Windows Batch one is incomplete because I could not find a way to extract the digits from
the start of the filename using regex. Perhaps it's possible to do it using findstr but googling didn't turn up much of
use.

### What else is in the box ###

* .sql/ contains both a set of 5 sample migrations and a script to create the database that I used for testing
* I have also provided Vagrantfile that spawns a Centos 7.2 vm with mariadb installed. Again, used for testing

### Thoughts ###

Of course, I'd practically rather use something like Python or Ruby on Linux, or Powershell on Windows in a real scenario. Batch's syntax is arcane and clunky, and Powershell 
is due to completely replace the old cmd.exe very soon. Bash syntax is arguably not much friendlier but I was able to get it working because there seems to be a lot more to google. 
A completely automated solution would use either the config management software (chef, ansible etc.) to do the migrations in an idempotent way, or better yet use an enterprise-grade database migration library like Flywaydb, and bake them into your applications so they always know what schema they should be running on and can either migrate themselves or fail fast.
