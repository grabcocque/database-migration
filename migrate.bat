@echo off
setlocal enableDelayedExpansion 

set USERNAME="dbuser"
set PASSWORD="dbpasswd"
set DBNAME="testdb"
set MIGRATIONDIR=".\sql\*.sql"
set REGEX="([0..9]+).*"

echo "Querying the database for the current version"

FOR /F "tokens=* USEBACKQ" %%F IN (`mysql -se "SELECT version FROM version WHERE id=1" --user=%USERNAME% --password=%PASSWORD% %DBNAME%`) DO (SET dbversion=%%F)

for /F %%script in ('dir /B /D %MIGRATIONDIR%') do (
    set filename=%~nscript
    echo processing %filename%

    REM here is the part where I would extract the numerical part of the filename using a
    REM regex, but I could not find how to do this in batch

    if (...TODO...) (

    echo Script version is %scriptversion%

       if %scriptversion% GTR %dbversion% (
          mysql --user=%USERNAME% --password=%PASSWORD% %DBNAME% < %%script
          if %scriptversion% GTR %maxdbversion% (
             set maxdbversion=%scriptversion%
             echo maxdbversion is now %maxdbversion%
          ) 
       )
    )
)

# finally update the database version to maxdbversion
mysql -e "UPDATE version SET version=!maxdbversion! WHERE id=1" --user=%USERNAME% --password=%PASSWORD% %DBNAME%
