#!/bin/bash
set -e
# script constants 

USERNAME="dbuser"
PASSWORD="dbpasswd"
DBNAME="testdb"
MIGRATIONDIR="sql/*.sql"

echo "Querying the database for the current version"

dbversion=$(mysql -se "SELECT version FROM version WHERE id=1" --user=$USERNAME --password=$PASSWORD $DBNAME)

echo "Current db version is $dbversion"

maxdbversion=$dbversion

# check and apply the migrations
for script in $MIGRATIONDIR; do
    filename=$(basename $script)
    echo "processing $filename"

    # extract the value from the start of the file name
    if [[ $filename =~  ([[:digit:]]+).* ]]
    then
        scriptversion="${BASH_REMATCH[1]}"

        echo "Script version is $scriptversion"

        # check the value against DBVERSION
        if [[ "$scriptversion" -gt "$dbversion" ]]
        then
            # 3. if it's higher than DBVERSION, run the script against the db, then update maxdbversion if needed
            mysql --user=$USERNAME --password=$PASSWORD $DBNAME < $script
            maxdbversion=$(( scriptversion > maxdbversion ? scriptversion : maxdbversion ))
            echo "maxdbversion is now $maxdbversion"
        fi
    fi
done

# finally update the database version to maxdbversion

echo "setting database version to $maxdbversion"

mysql -e "UPDATE version SET version=$maxdbversion WHERE id=1" --user=$USERNAME --password=$PASSWORD $DBNAME
